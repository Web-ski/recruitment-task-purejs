import { route } from "./router.js";

const url = "https:zwzt-zadanie.netlify.app/api/login";
localStorage.setItem("login", "false");

const post = (user, password) => {
  let login = { username: user, password: password };

  fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(login),
  })
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
      data.message === "Login success!" && (localStorage.login = user);
      data.message === "Login success!" &&
        location.replace(location.pathname + "#/success");
    });
};

route("/", "home", function () {
  let username = null;
  let password = null;
  this.where = "Home";
  this.info = "";

  this.$on("#username", "input", (e) => {
    username = e.target.value;
  });
  this.$on("#password", "input", (e) => {
    password = e.target.value;
  });
  this.$on(".form__button", "click", () => {
    username !== null && password !== null && (this.info = "");
    username !== null && password !== null
      ? post(username, password)
      : (this.info = "No username or password!");
    username = null;
    password = null;
    this.$refresh();
  });
});

const homeComponent = `
  <section class="home">
    <h1 class="section__title"><%= where %></h1>
    <form class="form">
      <legend class="form__title">Login</legend>
      <h4 class="form__info"><%= info %></h4>
      <label class="form__label" for="uname">Username</label>
      <input id="username" class="form__input" type="text" name="uname" required>
      <label class="form__label" for="psw">Password</label>
      <input id="password" class="form__input" type="password" name="password" required>
      <button class="form__button" type="submit">Submit</button>
    </form>
  </section>
`;

const addHomeComponent = () => {
  document.getElementById("home").innerHTML = homeComponent;
};

addHomeComponent();
