import { route } from "./router.js";

route("/success", "success", function () {
  this.where = "Success";
  this.who = `${localStorage.login}`;
  this.text = "You are now logged in.";
});

const successComponent = `
  <section class="success">
    <h1 class="section__title"><%= where %></h1>
    <article class="success__info">
      <h2 class="section__subtitle">Hi, <%= who %>! </h1>
      <h2 class="section__subtitle"><%= text %></h1>
    </article>
  </section>
`;

const addSuccessComponent = () => {
  document.getElementById("success").innerHTML = successComponent;
};

addSuccessComponent();
